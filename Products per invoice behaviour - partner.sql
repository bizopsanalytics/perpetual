-- Find the number of line items that a N2N and N2E 
-- add to their cart to help will decide whether they 
-- need to build a shopping cart that needs to handle 
-- multiple functions - Will Osteux



-- N2N check without marketplace addon for 2017
with mkt as
(
select invoice
from public.sale
where financial_year = 'FY2017'
and base_product = 'Marketplace Addon'
)
,
prodinv as 
(
select  invoice, 
        count(base_product) as products
from    public.sale
where   financial_year = 'FY2017'
--and     platform = 'Cloud'
and     sale_type = 'New to New'
--and     invoice not in (select invoice from mkt)
and     sold_via_partner = false
group by 1
order by 2 desc
)
select  products, 
        count(invoice) as inv
from    prodinv
group by 1
order by 1;


select sum(amount) as total, max(amount) as maxx, count(distinct invoice) as num
from public.sale
where financial_year = 'FY2017'
and   sold_via_partner = true
and   sale_type = 'New to New'
and   platform = 'Server'