-- Find the number of line items that a N2N and N2E 
-- add to their cart to help will decide whether they 
-- need to build a shopping cart that needs to handle 
-- multiple functions - Will Osteux

with prodinv as 
(
select  invoice, 
        count(base_product) as products
from    public.sale
where   financial_year = 'FY2016'
and     platform = 'Server'
group by 1
--order by 2 desc
)
select  products, 
        count(invoice) as inv
from    prodinv
group by 1
order by 1

;
-- check without marketplace addon
with prodinv as 
(
select  invoice, 
        count(base_product) as products
from    public.sale
where   financial_year = 'FY2016'
--and     platform = 'Cloud'
and     base_product not in ('Marketplace Addon')
group by 1
--order by 2 desc
)
select  products, 
        count(invoice) as inv
from    prodinv
group by 1
order by 1



;
-- to investigate invoice
select  * 
from    public.sale
where   invoice = 'AT-15295217'

;

-- check without marketplace addon for 2017
with mkt as
(
select invoice
from public.sale
where financial_year = 'FY2017'
and base_product = 'Marketplace Addon'
)
--,
--prodinv as 
--(
select  invoice, 
        count(base_product) as products
from    public.sale
where   financial_year = 'FY2017'
--and     platform = 'Cloud'
and     invoice not in (select invoice from mkt)
group by 1
order by 2 desc
)
select  products, 
        count(invoice) as inv
from    prodinv
group by 1
order by 1

;



-- N2N check without marketplace addon for 2017
with mkt as
(
select invoice
from public.sale
where financial_year = 'FY2017'
and base_product = 'Marketplace Addon'
)
,
prodinv as 
(
select  invoice, 
        count(base_product) as products
from    public.sale
where   financial_year = 'FY2017'
--and     platform = 'Cloud'
and     sale_type = 'New to New'
and     invoice not in (select invoice from mkt)
group by 1
order by 2 desc
)
select  products, 
        count(invoice) as inv
from    prodinv
group by 1
order by 1