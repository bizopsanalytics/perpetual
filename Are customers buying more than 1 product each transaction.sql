-- are customers buying one product at a time?
-- investigative
with first_date as 
(
     select     email, 
                min(date) as min_date
     from       public.sale
     where      platform = 'Server'
     group by 1
)     
        
select  a.email, 
        a.date,
        a.invoice,        
        a.sale_type,                
        --count(distinct a.base_product) as prod_count
        a.base_product,
        platform,
        case when a.date = b.min_date then 1 else 0 end as "land invoice?",
from    public.sale as a
left join first_date as b on a.email = b.email
where    platform = 'Server'    
and      financial_year = 'FY2017'
--group by 1,2,3,4,5,6,7
order by 1,2,3
;       
 
-- are customers buying one product at a time?
-- summary
with first_date as 
(
     select     email, 
                min(date) as min_date
     from       public.sale
     where      platform = 'Server'
     group by 1
)     
, customer as (       
select  a.email, 
        a.date,
        a.invoice, 
        case when a.date = b.min_date then 1 else 0 end as "first_server_invoice" ,      
        count(distinct a.sale_type) as actions_taken,                
        count(distinct a.base_product) as prod_count,
        count(distinct a.platform) as platforms      
from    public.sale as a
left join first_date as b on a.email = b.email
where   platform = 'Server'    
and     financial_year = 'FY2017'
group by 1,2,3,4
order by 1,2,3
)

select  first_server_invoice,
        actions_taken, 
        prod_count,
        count(distinct email)
from customer
group by 1,2,3,4
order by 1,2,3,4

;       